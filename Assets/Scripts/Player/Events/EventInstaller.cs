﻿using DontGoAlone.Player.Events.Die;
using DontGoAlone.Player.Events.Idle;
using DontGoAlone.Player.Events.TakeDamage;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Events
{
    class EventInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Debug.Log("Installing Event bindings");

            var audioSource = Container.ResolveId<AudioSource>("player.audio.source");

            BindEventDatabus();

            var databus = Container.Resolve<EventDatabus>();
            var playerState = Container.Resolve<PlayerState>();

            BindDieEventSound(audioSource);
            BindDieEventPublisher(databus, playerState);
            BindIdleEventPublisher(databus);

            BindTakeDamageEventSound(audioSource);
            BindTakeDamageEventPublisher(databus, playerState);
        }

        private void BindEventDatabus()
        {
            Debug.Log("Binding EventDatabus...");
            Container
                .Bind<EventDatabus>()
                .FromInstance(new EventDatabus(new Dictionary<Event, IEventPublisher>()));
        }

        private void BindTakeDamageEventPublisher(EventDatabus databus, PlayerState state)
        {
            Debug.Log("Binding TakeDamageEventPublisher...");
            Container
                .Bind<IEventPublisher>()
                .WithId("take.damage.event.publisher")
                .FromInstance(new TakeDamageEventPublisher(databus, state));
        }

        private void BindTakeDamageEventSound(AudioSource source)
        {
            Debug.Log("Binding TakeDamageEventSound...");
            var clips = Resources.LoadAll("Male/TakeDamage", typeof(AudioClip))
                .Select(clip => (AudioClip) clip)
                .ToArray();
            Container
                .Bind<TakeDamageEventSound>()
                .FromInstance(new TakeDamageEventSound(source, clips));
        }

        private void BindDieEventPublisher(EventDatabus databus, PlayerState state)
        {
            Debug.Log("Binding DieEventPublisher...");
            Container
                .Bind<IEventPublisher>()
                .WithId("die.event.publisher")
                .FromInstance(new DieEventPublisher(databus, state));
        }

        private void BindDieEventSound(AudioSource source)
        {
            Debug.Log("Binding DieEventSound...");
            var clips = Resources.LoadAll("Male/Die", typeof(AudioClip))
                .Select(clip => (AudioClip)clip)
                .ToArray();
            Container
                .Bind<DieEventSound>()
                .FromInstance(new DieEventSound(source, clips));
        }

        private void BindIdleEventPublisher(EventDatabus databus)
        {
            Debug.Log("Binding IdleEventPublisher...");
            Container
                .Bind<IEventPublisher>()
                .WithId("idle.event.publisher")
                .FromInstance(new IdleEventPublisher(databus));
        }
    }
}
