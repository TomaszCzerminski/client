﻿namespace DontGoAlone.Player.Events
{
    public interface IEventPublisher
    {
        void AddSubscriber(IEventSubscriber subscriber);
        void Publish();
        bool CanPublish();
    }
}
