﻿namespace DontGoAlone.Player.Events.Die
{
    interface IDieEventSubscriber : IEventSubscriber
    {
        void HandleDieEvent();
    }
}
