﻿namespace DontGoAlone.Player.Events.Die
{
    delegate void Handle();

    class DieEventPublisher : IEventPublisher
    {
        private event Handle Die;

        private PlayerState state;

        public DieEventPublisher(EventDatabus databus, PlayerState state)
        {
            databus.Register(Event.Die, this);
            this.state = state;
        }

        public void AddSubscriber(IEventSubscriber subscriber)
        {
            Die += (subscriber as IDieEventSubscriber).HandleDieEvent;
        }

        public void Publish()
        {
            Die();
        }

        public bool CanPublish()
        {
            return !state.IsDead;
        }
    }
}
