﻿using UnityEngine;

namespace DontGoAlone.Player.Events.Die
{
    class DieEventSound : AbstractSound
    {
        public DieEventSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
