﻿using UnityEngine;

namespace DontGoAlone.Player.Events.TakeDamage
{
    class TakeDamageEventSound : AbstractSound
    {
        public TakeDamageEventSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
