﻿using UnityEngine;

namespace DontGoAlone.Player.Events.TakeDamage
{
    delegate void Handle(byte damage);

    class TakeDamageEventPublisher : IEventPublisher
    {
        private event Handle TakeDamage;

        private readonly PlayerState state;

        public TakeDamageEventPublisher(EventDatabus databus, PlayerState state)
        {
            databus.Register(Event.TakeDamage, this);
            this.state = state;
        }

        public void AddSubscriber(IEventSubscriber subscriber)
        {
            TakeDamage += (subscriber as ITakeDamageEventSubscriber).HandleTakeDamageEvent;
        }

        public void Publish()
        {
            TakeDamage(10);
        }

        public bool CanPublish()
        {
            return !state.IsDead;
        }
    }
}
