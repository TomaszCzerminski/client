﻿namespace DontGoAlone.Player.Events.TakeDamage
{
    interface ITakeDamageEventSubscriber : IEventSubscriber 
    {
        void HandleTakeDamageEvent(byte damage);
    }
}
