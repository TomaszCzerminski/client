﻿namespace DontGoAlone.Player.Events.Idle
{
    delegate void Handle();

    class IdleEventPublisher : IEventPublisher
    {
        private event Handle Idle;

        private readonly PlayerState state;

        public IdleEventPublisher(EventDatabus databus)
        {
            databus.Register(Event.Idle, this);
        }

        public void AddSubscriber(IEventSubscriber subscriber)
        {
            Idle += (subscriber as IIdleEventSubscriber).HandleIdleEvent;
        }

        public void Publish()
        {
            Idle();
        }

        public bool CanPublish()
        {
            return !state.IsDead;
        }
    }
}
