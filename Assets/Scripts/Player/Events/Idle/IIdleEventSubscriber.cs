﻿namespace DontGoAlone.Player.Events.Idle
{
    interface IIdleEventSubscriber : IEventSubscriber
    {
        void HandleIdleEvent();
    }
}