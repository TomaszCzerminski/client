﻿using System.Collections.Generic;
using UnityEngine;

namespace DontGoAlone.Player.Events
{
    public class EventDatabus
    {
        private readonly IDictionary<Event, IEventPublisher> publishers;

        public EventDatabus(IDictionary<Event, IEventPublisher> publishers)
        {
            this.publishers = publishers;
        }

        public IEventPublisher GetPublisher(Event @event)
        {
            return publishers[@event];
        }

        public void Register(Event @event, IEventPublisher publisher)
        {
            Debug.Log("Registering: event=" + @event + ", publisher=" + publisher.GetType());
            publishers.Add(@event, publisher);
        }

        public void Subscribe(Event @event, IEventSubscriber subscriber)
        {
            Debug.Log("Subscribing: event=" + @event + ", subscriber=" + subscriber.GetType());
            publishers[@event].AddSubscriber(subscriber);
        }
    }
}