﻿namespace DontGoAlone.Player.Events
{
    public enum Event
    {
        Die, Idle, TakeDamage, EmptyCartridge
    }
}
