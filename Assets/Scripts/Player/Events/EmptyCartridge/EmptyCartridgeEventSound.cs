﻿using UnityEngine;

namespace DontGoAlone.Player.Events.EmptyCartridge
{
    class EmptyCartridgeEventSound : AbstractSound
    {
        public EmptyCartridgeEventSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
