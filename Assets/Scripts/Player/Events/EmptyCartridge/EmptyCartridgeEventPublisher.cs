﻿namespace DontGoAlone.Player.Events.EmptyCartridge
{
    delegate void Handle();

    class EmptyCartridgeEventPublisher : IEventPublisher
    {
        private event Handle EmptyCartridge;

        public EmptyCartridgeEventPublisher(EventDatabus databus)
        {
            databus.Register(Event.EmptyCartridge, this);
        }

        public void AddSubscriber(IEventSubscriber subscriber)
        {

        }

        public bool CanPublish()
        {
            throw new System.NotImplementedException();
        }

        public void Publish()
        {
            throw new System.NotImplementedException();
        }
    }
}
