﻿using DontGoAlone.Enemy;
using DontGoAlone.Items;
using DontGoAlone.Player.Actions;
using DontGoAlone.Player.Actions.Interact;
using DontGoAlone.Player.Actions.Jump;
using DontGoAlone.Player.Actions.Move.MoveDown;
using DontGoAlone.Player.Actions.Move.MoveLeft;
using DontGoAlone.Player.Actions.Move.MoveRight;
using DontGoAlone.Player.Actions.Move.MoveUp;
using DontGoAlone.Player.Actions.Shoot;
using DontGoAlone.Player.Actions.ToggleAimingMode;
using DontGoAlone.Player.Actions.ToggleRunMode;
using DontGoAlone.Player.Actions.ToggleStealthMode;
using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.Die;
using DontGoAlone.Player.Events.Idle;
using DontGoAlone.Player.Events.TakeDamage;
using DontGoAlone.Utils;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player
{
    class PlayerStateMutator :
        MonoBehaviour,
        IShootActionSubscriber,
        IToggleRunModeActionSubscriber,
        IToggleStealthModeActionSubscriber,
        IToggleAimingModeActionSubscriber,
        IMoveRightActionSubscriber,
        IMoveLeftActionSubscriber,
        IMoveUpActionSubscriber,
        IMoveDownActionSubscriber,
        IInteractActionSubscriber,
        IJumpActionSubscriber,
        IIdleEventSubscriber,
        IDieEventSubscriber,
        ITakeDamageEventSubscriber
    {
        private DieEventPublisher dieEventPublisher;
        private PlayerState state;

        private void FixedUpdate()
        {
            CheckIfGrounded();
            UpdateCoordinates();
            if (state.IsIdle)
            {
                state.IsShooting = false;
            }
            if (state.Stamina <= 0)
            {
                if (dieEventPublisher.CanPublish())
                    dieEventPublisher.Publish();
            }
        }

        public void HandleJumpAction()
        {
            state.IsGrounded = false;
        }

        private void Move(Direction direction)
        {
            if (!(state.FacingDirection == direction) && !state.IsInAimingMode)
            {
                state.FacingDirection = direction;
            }
            state.IsMoving = true;
        }

        public void HandleMoveLeftAction()
        {
            if (!state.IsDead)
            {
                if (!state.IsInAimingMode)
                {
                    transform.LookAt(new Vector3(transform.position.x, transform.position.y, -1000));
                }
                Move(Direction.Left);
            }
        }

        public void HandleMoveRightAction()
        {
            if (!state.IsDead)
            {
                if (!state.IsInAimingMode)
                {
                    transform.LookAt(new Vector3(transform.position.x, transform.position.y, 1000));
                }
                Move(Direction.Right);
            }
        }

        public void HandleMoveUpAction()
        {
            if (!state.IsDead)
            {
                if (!state.IsInAimingMode)
                {
                    transform.LookAt(new Vector3(-1000, transform.position.y, transform.position.z));
                }
                Move(Direction.Up);
            }
        }

        public void HandleMoveDownAction()
        {
            if (!state.IsDead)
            {
                if (!state.IsInAimingMode)
                {
                    transform.LookAt(new Vector3(1000, transform.position.y, transform.position.z));
                }
                Move(Direction.Down);
            }
        }

        private void UpdateCoordinates()
        {
            state.Coordinates.X = transform.position.z;
            state.Coordinates.Y = transform.position.y;
        }

        private void CheckIfGrounded()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 0.3f))
            {
                state.IsGrounded = hit.collider.CompareTag("Floor");
            }
            else
            {
                state.IsGrounded = false;
            }
        }

        public void HandleEnterRunModeAction()
        {
            state.IsInRunningMode = true;
        }

        public void HandleExitRunModeAction()
        {
            state.IsInRunningMode = false;
        }

        public void HandleEnterStealthModeAction()
        {
            state.IsInStealthMode = true;
        }

        public void HandleExitStealthModeAction()
        {
            state.IsInStealthMode = false;
        }

        public void HandleEnterAimingModeAction()
        {
            state.IsInAimingMode = true;
        }

        public void HandleExitAimingModeAction()
        {
            state.IsInAimingMode = false;
        }

        public void HandleIdleEvent()
        {
            state.IsMoving = false;
            state.IsShooting = false;
        }

        public void HandleInteractAction(AbstractInteractable interactable)
        {
            interactable.Interact();
        }

        public void HandleDieEvent()
        {
            state.IsDead = true;
        }

        public void HandleTakeDamageEvent(byte damage)
        {
            if (state.IsDead)
                return;
            if (state.Pain <= 0)
            {
                state.Stamina -= damage;
            }
            else
            {
                state.Pain -= damage;
            }
        }

        public void HandleShootAction(EnemyState enemy)
        { 
            if (true)
            {
                state.IsShooting = true;
            }
        }

        [Inject]
        public void Inject(
            ActionDatabus actionDatabus,
            EventDatabus eventDatabus,
            PlayerState state)
        {
            this.state = state;
            actionDatabus.Subscribe(Action.ToggleRunMode, this);
            actionDatabus.Subscribe(Action.ToggleAimingMode, this);
            actionDatabus.Subscribe(Action.MoveRight, this);
            actionDatabus.Subscribe(Action.MoveLeft, this);
            actionDatabus.Subscribe(Action.MoveUp, this);
            actionDatabus.Subscribe(Action.MoveDown, this);
            actionDatabus.Subscribe(Action.Jump, this);
            actionDatabus.Subscribe(Action.Interact, this);
            eventDatabus.Subscribe(Events.Event.Idle, this);
            eventDatabus.Subscribe(Events.Event.Die, this);
            eventDatabus.Subscribe(Events.Event.TakeDamage, this);
            actionDatabus.Subscribe(Action.Shoot, this);
            dieEventPublisher = eventDatabus.GetPublisher(Events.Event.Die) as DieEventPublisher;
        }
    }
}