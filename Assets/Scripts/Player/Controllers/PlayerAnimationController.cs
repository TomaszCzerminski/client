﻿using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.Die;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Controller
{
    class PlayerAnimationController : 
        MonoBehaviour,
        IDieEventSubscriber
    {
        private Animator animator;
        private PlayerState state;

        private void LateUpdate()
        {
            if (!state.IsDead)
            {
                animator.SetInteger("WeaponType_int", state.IsInAimingMode ? 1 : 0);
                animator.SetBool("Death_b", state.IsDead);
                animator.SetBool("Shoot_b", state.IsShooting);
                animator.SetBool("Jump_b", !state.IsGrounded);
                animator.SetFloat("Speed_f", CalculateMovement());
            }
        }

        private float CalculateMovement()
        {
            if (state.IsMoving)
            {
                return state.IsInRunningMode ? 0.6f : 0.3f;
            }

            return 0f;
        }

        public void HandleDieEvent()
        {
            animator.SetBool("Death_b", true);
        }

        [Inject]
        public void Inject(
            EventDatabus eventDatabus, 
            [Inject(Id = "player.animator")] Animator animator, 
            PlayerState state)
        {
            Debug.Log("Binding PlayerAnimationController...");
            this.animator = animator;
            this.state = state;
            eventDatabus.Subscribe(Events.Event.Die, this);
        }
    }
}