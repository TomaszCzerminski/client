﻿using DontGoAlone.Enemy;
using DontGoAlone.Player.Actions;
using DontGoAlone.Player.Actions.Jump;
using DontGoAlone.Player.Actions.Move;
using DontGoAlone.Player.Actions.Move.MoveDown;
using DontGoAlone.Player.Actions.Move.MoveLeft;
using DontGoAlone.Player.Actions.Move.MoveRight;
using DontGoAlone.Player.Actions.Move.MoveUp;
using DontGoAlone.Player.Actions.Shoot;
using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.Die;
using DontGoAlone.Player.Events.Idle;
using DontGoAlone.Player.Events.TakeDamage;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Controllers
{
    class PlayerAudioController :
        MonoBehaviour,
        IIdleEventSubscriber,
        IShootActionSubscriber,
        IJumpActionSubscriber,
        IMoveLeftActionSubscriber,
        IMoveRightActionSubscriber,
        IMoveUpActionSubscriber,
        IMoveDownActionSubscriber,
        ITakeDamageEventSubscriber,
        IDieEventSubscriber
    {
        private RunActionSound runActionSound;
        private WalkActionSound walkActionSound;
        private ShootActionSound shootActionSound;
        private TakeDamageEventSound takeDamageEventSound;
        private DieEventSound dieEventSound;
        private PlayerState playerState;

        public void HandleDieEvent()
        {
            if (!dieEventSound.IsPlaying())
                dieEventSound.Play();
        }

        public void HandleIdleEvent()
        {
            throw new System.NotImplementedException();
        }

        public void HandleJumpAction()
        {
            throw new System.NotImplementedException();
        }

        public void HandleMoveDownAction()
        {
            if (playerState.IsInRunningMode)
            {
                if (!runActionSound.IsPlaying())
                    runActionSound.Play();
            }
            else
            {
                if (!walkActionSound.IsPlaying())
                    walkActionSound.Play();
            }
        }

        public void HandleMoveLeftAction()
        {
            if (playerState.IsInRunningMode)
            {
                if (!runActionSound.IsPlaying())
                    runActionSound.Play();
            }
            else
            {
                if (!walkActionSound.IsPlaying())
                    walkActionSound.Play();
            }
        }

        public void HandleMoveRightAction()
        {
            if (playerState.IsInRunningMode)
            {
                if (!runActionSound.IsPlaying())
                    runActionSound.Play();
            }
            else
            {
                if (!walkActionSound.IsPlaying())
                    walkActionSound.Play();
            }
        }

        public void HandleMoveUpAction()
        {
            if (playerState.IsInRunningMode)
            {
                if (!runActionSound.IsPlaying())
                    runActionSound.Play();
            }
            else
            {
                if (!walkActionSound.IsPlaying())
                    walkActionSound.Play();
            }
        }

        public void HandleShootAction(EnemyState enemy)
        {
            shootActionSound.Play();
        }

        public void HandleTakeDamageEvent(byte damage)
        {
            if (!takeDamageEventSound.IsPlaying())
                takeDamageEventSound.Play();
        }

        [Inject]
        public void Inject(
            EventDatabus eventDatabus,
            ActionDatabus actionDatabus,
            PlayerState playerState,
            RunActionSound runActionSound,
            WalkActionSound walkActionSound,
            ShootActionSound shootActionSound,
            TakeDamageEventSound takeDamageEventSound,
            DieEventSound dieEventSound)
        {
            this.playerState = playerState;
            this.runActionSound = runActionSound;
            this.walkActionSound = walkActionSound;
            this.shootActionSound = shootActionSound;
            this.takeDamageEventSound = takeDamageEventSound;
            this.dieEventSound = dieEventSound;
            actionDatabus.Subscribe(Action.MoveDown, this);
            actionDatabus.Subscribe(Action.MoveUp, this);
            actionDatabus.Subscribe(Action.MoveLeft, this);
            actionDatabus.Subscribe(Action.MoveRight, this);
            actionDatabus.Subscribe(Action.Shoot, this);
            eventDatabus.Subscribe(Events.Event.Die, this);
            eventDatabus.Subscribe(Events.Event.TakeDamage, this);
        }
    }
}
