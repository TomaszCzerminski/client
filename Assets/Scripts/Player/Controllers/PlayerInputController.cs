﻿using DontGoAlone.Items.Container;
using DontGoAlone.Player.Actions;
using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.Idle;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Controller
{
    class PlayerInputController : MonoBehaviour
    {
        private PlayerState state;
        private IdleEventPublisher idleEventPublisher;
        private ActionDatabus actionDatabus;

        private void Update()
        {
            GetHoveredObject();
            foreach (var publisher in actionDatabus.GetAllPublishers())
            {
                if (publisher.CanPublish())
                {
                    publisher.Publish();
                    return;
                }
            }
            idleEventPublisher.Publish();
        }

        private void GetHoveredObject()
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.collider.CompareTag("Container"))
                {
                    state.HoveredInteractable = hit.collider.GetComponent<Container>();
                }
                else
                {
                    state.HoveredInteractable = null;
                }
            }
        }

        [Inject]
        public void Inject(
            ActionDatabus actionDatabus,
            EventDatabus eventDatabus,
            PlayerState state)
        {
            this.state = state;
            this.actionDatabus = actionDatabus;
            idleEventPublisher = eventDatabus.GetPublisher(Events.Event.Idle) as IdleEventPublisher;
        }
    }
}
