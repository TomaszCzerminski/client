﻿using DontGoAlone.Enemy;
using DontGoAlone.Player.Actions;
using DontGoAlone.Player.Actions.Jump;
using DontGoAlone.Player.Actions.Move.MoveDown;
using DontGoAlone.Player.Actions.Move.MoveLeft;
using DontGoAlone.Player.Actions.Move.MoveRight;
using DontGoAlone.Player.Actions.Move.MoveUp;
using DontGoAlone.Player.Actions.Shoot;
using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.Idle;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Controller
{
    class PlayerPhysicsController :
        MonoBehaviour,
        IIdleEventSubscriber,
        IShootActionSubscriber,
        IJumpActionSubscriber,
        IMoveLeftActionSubscriber,
        IMoveRightActionSubscriber,
        IMoveUpActionSubscriber,
        IMoveDownActionSubscriber
    {
        private Rigidbody rigidbody;
        private Rigidbody projectile;
        private PlayerState state;
        private float currentMovement;
        
        public void HandleJumpAction()
        {
            rigidbody.AddForce(Vector3.up * 4f, ForceMode.Impulse);
        }

        public void HandleMoveLeftAction()
        {
            if (!state.IsDead)
            {
                var rigidbody = GetComponent<Rigidbody>();
                rigidbody.AddForce(0, 0, state.IsInRunningMode ? -120f : -60f, ForceMode.Acceleration);
            }
        }

        public void HandleMoveRightAction()
        {
            if (!state.IsDead)
            {
                var rigidbody = GetComponent<Rigidbody>();
                rigidbody.AddForce(0, 0, state.IsInRunningMode ? 120f : 60f, ForceMode.Acceleration);
            }
        }

        public void HandleMoveUpAction()
        {
            if (!state.IsDead)
            {
                var rigidbody = GetComponent<Rigidbody>();
                rigidbody.AddForce(state.IsInRunningMode ? -120f : -60f, 0, 0, ForceMode.Acceleration);
            }
        }

        public void HandleMoveDownAction()
        {
            if (!state.IsDead)
            {
                var rigidbody = GetComponent<Rigidbody>();
                rigidbody.AddForce(state.IsInRunningMode ? 120f : 60f, 0, 0, ForceMode.Acceleration);
            }
        }

        public void HandleShootAction(EnemyState enemy)
        {
            var projectile = Instantiate(this.projectile, GameObject.FindGameObjectWithTag("Weapon").transform.position, Quaternion.identity);
            projectile.AddForce(0, 0, 30, ForceMode.Impulse);
        }

        public void HandleIdleEvent()
        {
            currentMovement = 0;
        }

        [Inject]
        public void Inject(
            ActionDatabus actionDatabus,
            EventDatabus eventDatabus,
            PlayerState state,
            [Inject(Id = "player.rigidbody")] Rigidbody rigidbody,
            [Inject(Id = "player.weapon.gun.bullet")] Rigidbody projectile)
        {
            actionDatabus.Subscribe(Action.Shoot, this);
            actionDatabus.Subscribe(Action.Jump, this);
            actionDatabus.Subscribe(Action.MoveLeft, this);
            actionDatabus.Subscribe(Action.MoveRight, this);
            actionDatabus.Subscribe(Action.MoveUp, this);
            actionDatabus.Subscribe(Action.MoveDown, this);
            eventDatabus.Subscribe(Events.Event.Idle, this);
            this.state = state;
            this.rigidbody = rigidbody;
            this.projectile = projectile;
        }
    }
}