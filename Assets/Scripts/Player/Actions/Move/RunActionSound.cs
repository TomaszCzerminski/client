﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Move
{
    class RunActionSound : AbstractSound
    {
        public RunActionSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
