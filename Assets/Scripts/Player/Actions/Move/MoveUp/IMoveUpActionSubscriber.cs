﻿namespace DontGoAlone.Player.Actions.Move.MoveUp
{
    interface IMoveUpActionSubscriber : IActionSubscriber
    {
        void HandleMoveUpAction();
    }
}
