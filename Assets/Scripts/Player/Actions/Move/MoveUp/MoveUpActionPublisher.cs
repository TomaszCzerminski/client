﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Move.MoveUp
{
    delegate void Handle();

    class MoveUpActionPublisher : IActionPublisher
    {
        private event Handle MoveUp;

        public MoveUpActionPublisher(ActionDatabus databus)
        {
            databus.Register(Action.MoveUp, this);
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            MoveUp += (subscriber as IMoveUpActionSubscriber).HandleMoveUpAction;
        }

        public bool CanPublish()
        {
            return Input.GetKey(KeyMap.Up);
        }

        public void Publish()
        {
            MoveUp();
        }
    }
}
