﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Move.MoveDown
{
    delegate void Handle();

    class MoveDownActionPublisher : IActionPublisher
    {
        private event Handle MoveDown;

        public MoveDownActionPublisher(ActionDatabus databus)
        {
            databus.Register(Action.MoveDown, this);
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            MoveDown += (subscriber as IMoveDownActionSubscriber).HandleMoveDownAction;
        }

        public bool CanPublish()
        {
            return Input.GetKey(KeyMap.Down);
        }

        public void Publish()
        {
            MoveDown();
        }
    }
}
