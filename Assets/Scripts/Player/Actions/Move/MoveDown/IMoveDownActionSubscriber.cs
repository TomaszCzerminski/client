﻿namespace DontGoAlone.Player.Actions.Move.MoveDown
{
    interface IMoveDownActionSubscriber : IActionSubscriber
    {
        void HandleMoveDownAction();
    }
}
