﻿using System;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Actions.Move.MoveRight
{
    delegate void Handle();

    class MoveRightActionPublisher : IActionPublisher
    {
        private event Handle MoveRight;
        
        public MoveRightActionPublisher(ActionDatabus databus)
        {
            databus.Register(Action.MoveRight, this);
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            MoveRight += (subscriber as IMoveRightActionSubscriber).HandleMoveRightAction;
        }

        public bool CanPublish()
        {
            return Input.GetKey(KeyMap.Right);
        }

        public void Publish()
        {
            MoveRight();
        }
    }
}
