﻿namespace DontGoAlone.Player.Actions.Move.MoveRight
{
    interface IMoveRightActionSubscriber : IActionSubscriber
    {
        void HandleMoveRightAction();
    }
}
