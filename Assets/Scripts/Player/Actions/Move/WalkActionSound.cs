﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Move
{
    class WalkActionSound : AbstractSound
    {
        public WalkActionSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
