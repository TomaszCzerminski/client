﻿namespace DontGoAlone.Player.Actions.Move.MoveLeft
{
    interface IMoveLeftActionSubscriber : IActionSubscriber
    {
        void HandleMoveLeftAction();
    }
}
