﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Move.MoveLeft
{
    delegate void Handle();

    class MoveLeftActionPublisher : IActionPublisher
    {
        private event Handle MoveLeft;

        public MoveLeftActionPublisher(ActionDatabus databus)
        {
            databus.Register(Action.MoveLeft, this);
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            MoveLeft += (subscriber as IMoveLeftActionSubscriber).HandleMoveLeftAction;
        }

        public bool CanPublish()
        {
            return Input.GetKey(KeyMap.Left);
        }

        public void Publish()
        {
            MoveLeft();
        }
    }
}
