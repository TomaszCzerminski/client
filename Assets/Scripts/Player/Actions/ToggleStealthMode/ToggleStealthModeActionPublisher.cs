﻿using System;
using UnityEngine;

namespace DontGoAlone.Player.Actions.ToggleStealthMode
{
    public delegate void Handle();

    class ToggleStealthModeActionPublisher : IActionPublisher
    {
        private event Handle Enter;
        private event Handle Exit;
        
        private readonly PlayerState state;

        public ToggleStealthModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            databus.Register(Action.ToggleStealthMode, this);
            this.state = state;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Enter += (subscriber as IToggleStealthModeActionSubscriber).HandleEnterStealthModeAction;
            Exit += (subscriber as IToggleStealthModeActionSubscriber).HandleExitStealthModeAction;
        }

        public void Publish()
        {
            if (state.IsInStealthMode)
            {
                Exit();
            }
            else
            {
                Enter();
            }
        }

        public bool CanPublish()
        {
            return Input.GetKeyDown(KeyMap.ToogleStealthMode);
        }
    }
}