﻿namespace DontGoAlone.Player.Actions.ToggleStealthMode
{
    interface IToggleStealthModeActionSubscriber : IActionSubscriber
    {
        void HandleEnterStealthModeAction();
        void HandleExitStealthModeAction();
    }
}