﻿using System;
using UnityEngine;

namespace DontGoAlone.Player.Actions
{
    interface IActionPublisher
    {
        void Publish();
        void AddSubscriber(IActionSubscriber subscriber);
        bool CanPublish();
    }
}
