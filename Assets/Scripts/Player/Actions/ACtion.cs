﻿namespace DontGoAlone.Player.Actions
{
    enum Action
    {
        Interact,
        Jump,
        MoveUp,
        MoveDown,
        MoveRight,
        MoveLeft,
        SelectEnemy,
        SelectInteractable,
        Shoot,
        ToggleRunMode,
        ToggleStealthMode,
        ToggleAimingMode
    }
}
