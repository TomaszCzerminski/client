﻿using DontGoAlone.Player.Actions.Interact;
using DontGoAlone.Player.Actions.Jump;
using DontGoAlone.Player.Actions.Move;
using DontGoAlone.Player.Actions.Move.MoveDown;
using DontGoAlone.Player.Actions.Move.MoveLeft;
using DontGoAlone.Player.Actions.Move.MoveRight;
using DontGoAlone.Player.Actions.Move.MoveUp;
using DontGoAlone.Player.Actions.Shoot;
using DontGoAlone.Player.Actions.ToggleAimingMode;
using DontGoAlone.Player.Actions.ToggleRunMode;
using DontGoAlone.Player.Actions.ToggleStealthMode;
using DontGoAlone.Utils;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Player.Actions
{
    class ActionInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Debug.Log("Installing Action bindings");

            BindActionDatabus();

            var player = GameObject
                .FindGameObjectWithTag("Player");
            var databus = Container.Resolve<ActionDatabus>();
            var state = player.GetComponent<PlayerState>();
            var audioSource = Container.ResolveId<AudioSource>("player.audio.source");
            
            BindInteractActionPublisher(databus, state);
            BindJumpActionPublisher(databus, state);
            BindMoveLeftActionPublisher(databus);
            BindMoveRightActionPublisher(databus);
            BindMoveUpActionPublisher(databus);
            BindMoveDownActionPublisher(databus);
            BindShootSound(audioSource);
            BindShootActionPublisher(databus, state);
            BindToggleRunModeActionPublisher(databus, state);
            BindToggleStealthModeActionPublisher(databus, state);
            BindToggleAimingModeActionPublisher(databus, state);
            BindRunActionSound(audioSource);
            BindWalkActionSound(audioSource);
        }

        private void BindRunActionSound(AudioSource source)
        {
            var clips = Resources.LoadAll("Run", typeof(AudioClip))
                .Select(clip => (AudioClip)clip)
                .ToArray();
            Container
                .Bind<RunActionSound>()
                .FromInstance(new RunActionSound(source, clips));
        }

        private void BindWalkActionSound(AudioSource source)
        {
            var clips = Resources.LoadAll("Walk", typeof(AudioClip))
                .Select(clip => (AudioClip)clip)
                .ToArray();
            Container
                .Bind<WalkActionSound>()
                .FromInstance(new WalkActionSound(source, clips));
        }

        private void BindActionDatabus()
        {
            Debug.Log("Binding ActionDatabus...");
            Container
                .Bind<ActionDatabus>()
                .FromInstance(new ActionDatabus(new Dictionary<Action, IActionPublisher>()));
        }

        private void BindInteractActionPublisher(ActionDatabus databus, PlayerState state)
        {
            Debug.Log("Binding InteractActionPublisher...");
            Container
                 .Bind<IActionPublisher>()
                 .WithId("interact.action.publisher")
                 .FromInstance(new InteractActionPublisher(databus, state));
        }

        private void BindJumpActionPublisher(ActionDatabus databus, PlayerState state)
        {
            Debug.Log("Binding JumpActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("jump.action.publisher")
                .FromInstance(new JumpActionPublisher(databus, state));
        }

        private void BindMoveLeftActionPublisher(ActionDatabus databus)
        {
            Debug.Log("Binding MoveLeftActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("move.left.action.publisher")
                .FromInstance(new MoveLeftActionPublisher(databus));
        }

        private void BindMoveRightActionPublisher(ActionDatabus databus)
        {
            Debug.Log("Binding MoveRightActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("move.right.action.publisher")
                .FromInstance(new MoveRightActionPublisher(databus));
        }

        private void BindMoveUpActionPublisher(ActionDatabus databus)
        {
            Debug.Log("Binding MoveUpActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("move.up.action.publisher")
                .FromInstance(new MoveUpActionPublisher(databus));
        }

        private void BindMoveDownActionPublisher(ActionDatabus databus)
        {
            Debug.Log("Binding MoveDownActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("move.down.action.publisher")
                .FromInstance(new MoveDownActionPublisher(databus));
        }
        
        private void BindShootActionPublisher(ActionDatabus databus, PlayerState state)
        {
            var executor = GameObject
                .FindGameObjectWithTag("Scene")
                .GetComponent<CoroutineExecutor>();
            Debug.Log("Binding ShootActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("shoot.action.publisher")
                .FromInstance(new ShootActionPublisher(databus, executor, state));
        }

        public void BindShootSound(AudioSource source)
        {
            Debug.Log("Binding ShootSound...");

            var clips = Resources.LoadAll("Pistol/Sounds/Shoot", typeof(AudioClip))
                .Select(clip => (AudioClip) clip)
                .ToArray();
            Container
                .Bind<ShootActionSound>()
                .FromInstance(new ShootActionSound(source, clips));
        }

        private void BindToggleRunModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            Debug.Log("Binding ToggleRunModeActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("toggle.run.mode.action.publisher")
                .FromInstance(new ToggleRunModeActionPublisher(databus, state));
        }

        private void BindToggleStealthModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            Debug.Log("Binding ToggleStealthModeActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("toggle.stealth.mode.action.publisher")
                .FromInstance(new ToggleStealthModeActionPublisher(databus, state));
        }

        private void BindToggleAimingModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            Debug.Log("Binding ToggleAimingModeActionPublisher...");
            Container
                .Bind<IActionPublisher>()
                .WithId("toggle.aiming.mode.action.publisher")
                .FromInstance(new ToggleAimingModeActionPublisher(databus, state));
        }
    }
}
