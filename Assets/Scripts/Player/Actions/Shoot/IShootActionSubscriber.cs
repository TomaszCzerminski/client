﻿using DontGoAlone.Enemy;

namespace DontGoAlone.Player.Actions.Shoot
{
    interface IShootActionSubscriber : IActionSubscriber
    {
        void HandleShootAction(EnemyState enemy);
    }
}
