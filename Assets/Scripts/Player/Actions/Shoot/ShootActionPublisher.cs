﻿using DontGoAlone.Enemy;
using DontGoAlone.Utils;
using System.Collections;
using UnityEngine;

namespace DontGoAlone.Player.Actions.Shoot
{
    public delegate void Handle(EnemyState enemy);

    class ShootActionPublisher : IActionPublisher
    {
        private event Handle Shoot;
        
        private readonly PlayerState state;
        private CoroutineExecutor CoroutineExecutor { get; set; }
        private bool CanShoot { get; set; }
        private WaitForSeconds AttackInterval { get; set; }

        public ShootActionPublisher(
            ActionDatabus databus, 
            CoroutineExecutor executor, 
            PlayerState state
        ) {
            databus.Register(Action.Shoot, this);
            this.state = state;
            CoroutineExecutor = executor;
            AttackInterval = new WaitForSeconds(1);
            CanShoot = true;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Shoot += (subscriber as IShootActionSubscriber).HandleShootAction;
        }

        public void Publish()
        {
            CanShoot = false;
            Shoot(state.HoveredEnemy);
            CoroutineExecutor.Execute(WaitUntilNextShot());
        }

        public bool CanPublish()
        {
            return state.IsInAimingMode 
                && state.HoveredEnemy != null 
                && CanShoot 
                && Input.GetKey(KeyMap.Shoot);
        }

        private IEnumerator WaitUntilNextShot()
        {
            yield return AttackInterval;
            CanShoot = true;
        }
    }
}
