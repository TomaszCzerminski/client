﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Shoot
{
    class ShootActionSound : AbstractSound
    {
        public ShootActionSound(AudioSource source, AudioClip[] clips) : base(source, clips)
        {
        }
    }
}
