﻿using DontGoAlone.Items;
using UnityEngine;

namespace DontGoAlone.Player.Actions.Interact
{
    delegate void Handle(AbstractInteractable interactable);

    class InteractActionPublisher : IActionPublisher
    {
        private event Handle Interact;
        private readonly PlayerState state;

        public InteractActionPublisher(ActionDatabus databus, PlayerState state)
        {
            databus.Register(Action.Interact, this);
            this.state = state;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Interact += (subscriber as IInteractActionSubscriber).HandleInteractAction;
        }

        public void Publish()
        {
            Interact(state.HoveredInteractable);
        }

        public bool CanPublish()
        {
            return state.HoveredInteractable != null && Input.GetKeyDown(KeyMap.Interact);
        }
    }
}
