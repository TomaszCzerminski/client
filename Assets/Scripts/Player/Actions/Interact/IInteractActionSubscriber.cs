﻿using DontGoAlone.Items;

namespace DontGoAlone.Player.Actions.Interact
{
    interface IInteractActionSubscriber : IActionSubscriber
    {
        void HandleInteractAction(AbstractInteractable interactable);
    }
}
 