﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DontGoAlone.Player.Actions
{
    class ActionDatabus
    {
        private readonly IDictionary<Action, IActionPublisher> publishers;

        public ActionDatabus(IDictionary<Action, IActionPublisher> publishers)
        {
            this.publishers = publishers;
        }

        public IList<IActionPublisher> GetAllPublishers()
        {
            return publishers.Values.ToList();
        }

        public void Register(Action action, IActionPublisher publisher)
        {
            Debug.Log("Registering: action=" + action + ", publisher=" + publisher.GetType());
            publishers.Add(action, publisher);
        }

        public void Subscribe(Action action, IActionSubscriber subscriber)
        {
            try
            {
                Debug.Log("Subscribing: action=" + action + ", subscriber=" + subscriber.GetType());
                publishers[action].AddSubscriber(subscriber);
            }
            catch (KeyNotFoundException)
            {
                Debug.Log("Action=" + action + " has not been registered!");
                Debug.Log("Registered actions:");
                foreach(var publisher in publishers)
                {
                    Debug.Log(publisher.Key);
                }
            }
        }
    }
}