﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.Jump
{
    public delegate void Handle();

    class JumpActionPublisher : IActionPublisher
    {
        private event Handle Jump;
        
        private readonly PlayerState state;

        public JumpActionPublisher(ActionDatabus databus, PlayerState state)
        {
            databus.Register(Action.Jump, this);
            this.state = state;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Jump += (subscriber as IJumpActionSubscriber).HandleJumpAction;
        }

        public void Publish()
        {
            Debug.Log("Jumping...");
            Jump();
        }

        public bool CanPublish()
        {
            return state.IsGrounded && Input.GetKeyDown(KeyMap.Jump);
        }
    }
}
