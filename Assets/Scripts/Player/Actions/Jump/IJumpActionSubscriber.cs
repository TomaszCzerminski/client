﻿namespace DontGoAlone.Player.Actions.Jump
{
    interface IJumpActionSubscriber : IActionSubscriber
    {
        void HandleJumpAction();
    }
}
