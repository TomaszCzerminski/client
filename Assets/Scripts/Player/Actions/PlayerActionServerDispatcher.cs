﻿using DontGoAlone.Connectivity;
using DontGoAlone.Enemy;
using DontGoAlone.Player.Actions.Move.MoveDown;
using DontGoAlone.Player.Actions.Move.MoveLeft;
using DontGoAlone.Player.Actions.Move.MoveRight;
using DontGoAlone.Player.Actions.Move.MoveUp;
using DontGoAlone.Player.Actions.Shoot;
using UnityEngine;

namespace DontGoAlone.Player.Actions
{
    class PlayerActionServerDispatcher :
        MonoBehaviour,
        IShootActionSubscriber,
        IMoveLeftActionSubscriber,
        IMoveRightActionSubscriber,
        IMoveUpActionSubscriber,
        IMoveDownActionSubscriber
    {
        private IServerConnector connector;

        public void HandleMoveDownAction()
        {
            connector.Send();
        }

        public void HandleMoveLeftAction()
        {
            connector.Send();
        }

        public void HandleMoveRightAction()
        {
            connector.Send();
        }

        public void HandleMoveUpAction()
        {
            connector.Send();
        }

        public void HandleShootAction(EnemyState enemy)
        {
            connector.Send();
        }
    }
}
