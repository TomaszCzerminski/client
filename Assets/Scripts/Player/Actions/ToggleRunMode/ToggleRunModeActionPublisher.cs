﻿using System;
using UnityEngine;

namespace DontGoAlone.Player.Actions.ToggleRunMode
{
    public delegate void Handle();

    class ToggleRunModeActionPublisher : IActionPublisher
    {
        private event Handle Enter;
        private event Handle Exit;
        
        private readonly PlayerState state;

        public ToggleRunModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            databus.Register(Action.ToggleRunMode, this);
            this.state = state;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Enter += (subscriber as IToggleRunModeActionSubscriber).HandleEnterRunModeAction;
            Exit += (subscriber as IToggleRunModeActionSubscriber).HandleExitRunModeAction;
        }

        public void Publish()
        {
            if (state.IsInRunningMode)
            {
                Exit();
            } else
            {
                Enter();
            }
        }

        public bool CanPublish()
        {
            return Input.GetKeyDown(KeyMap.ToogleRunMode);
        }
    }
}
