﻿namespace DontGoAlone.Player.Actions.ToggleRunMode
{
    interface IToggleRunModeActionSubscriber : IActionSubscriber
    {
        void HandleEnterRunModeAction();
        void HandleExitRunModeAction();
    }
}
