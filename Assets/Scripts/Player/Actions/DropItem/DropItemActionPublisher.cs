﻿using System;

namespace DontGoAlone.Player.Actions.DropItem
{
    delegate void Handle(Guid guid);

    class DropItemActionPublisher : IActionPublisher
    {
        private event Handle DropItem;

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            throw new System.NotImplementedException();
        }

        public bool CanPublish()
        {
            throw new System.NotImplementedException();
        }

        public void Publish()
        {
            throw new System.NotImplementedException();
        }
    }
}
