﻿using UnityEngine;

namespace DontGoAlone.Player.Actions.ToggleAimingMode
{
    public delegate void Handle();

    class ToggleAimingModeActionPublisher : IActionPublisher
    {
        private event Handle Enter;
        private event Handle Exit;
        
        private readonly PlayerState state;

        public ToggleAimingModeActionPublisher(ActionDatabus databus, PlayerState state)
        {
            databus.Register(Action.ToggleAimingMode, this);
            this.state = state;
        }

        public void AddSubscriber(IActionSubscriber subscriber)
        {
            Enter += (subscriber as IToggleAimingModeActionSubscriber).HandleEnterAimingModeAction;
            Exit += (subscriber as IToggleAimingModeActionSubscriber).HandleExitAimingModeAction;
        }

        public void Publish()
        {
            if (state.IsInAimingMode)
            {
                Exit();
            }
            else
            {
                Enter();
            }
        }

        public bool CanPublish()
        {
            return Input.GetKeyDown(KeyMap.ToogleAimingMode);
        }
    }
}
