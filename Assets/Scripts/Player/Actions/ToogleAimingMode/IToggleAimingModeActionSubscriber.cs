﻿namespace DontGoAlone.Player.Actions.ToggleAimingMode
{
    interface IToggleAimingModeActionSubscriber : IActionSubscriber
    {
        void HandleEnterAimingModeAction();
        void HandleExitAimingModeAction();
    }
}