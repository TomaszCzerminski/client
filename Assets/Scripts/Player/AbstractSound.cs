﻿using UnityEngine;

namespace DontGoAlone.Player
{
    abstract class AbstractSound
    {
        protected readonly AudioSource source;
        protected readonly AudioClip[] clips;

        public void Play()
        {
            var index = Random.Range(0, clips.Length);
            source.clip = clips[index];
            source.Play();
        }

        public bool IsPlaying()
        {
            return source.isPlaying;
        }

        protected AbstractSound(AudioSource source, AudioClip[] clips)
        {
            this.source = source;
            this.clips = clips;
        }
    }
}
