﻿using DontGoAlone.Enemy;
using DontGoAlone.Items;
using DontGoAlone.Utils;
using UnityEngine;

namespace DontGoAlone.Player
{
    public class PlayerState : MonoBehaviour
    {
        public PlayerState()
        {
            Coordinates = new Coordinates();
            IsIdle = true;
            IsGrounded = true;
            Pain = 100;
            Stamina = 100;
            HoveredInteractable = null;
            Tag = "Player";
        }

        public string Tag { get; set; }
        public WeaponState Weapon { get; set; }
        public bool IsDead { get; set; }
        public bool IsMoving { get; set; }
        public bool IsIdle { get; set; }
        public bool IsShooting { get; set; }
        public bool IsInRunningMode { get; set; }
        public Direction FacingDirection { get; set; }
        public bool IsInAimingMode { get; set; }
        public bool IsInStealthMode { get; set; }
        public bool IsGrounded { get; set; }
        public byte Pain { get; set; }
        public byte Stamina { get; set; }
        public AbstractInteractable HoveredInteractable { get; set; }
        public EnemyState HoveredEnemy { get; set; }
        public Coordinates RespawnCoordinates { get; set; }
        public Coordinates Coordinates { get; set; }
        
    }
}