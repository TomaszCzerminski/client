﻿using UnityEngine;
using Zenject;

namespace DontGoAlone.Player
{
    class PlayerComponentsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            var player = GameObject.FindGameObjectWithTag("Player");

            BindPlayerAnimator(player);
            BindPlayerState(player);
            BindPlayerAudioSource(player);
            BindPlayerRigidbody(player);
            BindPlayerGunBullet();
        }

        private void BindPlayerAudioSource(GameObject player)
        {
            Debug.Log("Binding PlayerAudioSource...");
            Container
                .Bind<AudioSource>()
                .WithId("player.audio.source")
                .FromInstance(player.GetComponent<AudioSource>());
        }

        private void BindPlayerAnimator(GameObject player)
        {
            Debug.Log("Binding PlayerAnimator...");
            Container
                .Bind<Animator>()
                .WithId("player.animator")
                .FromInstance(player.GetComponent<Animator>());
        }

        private void BindPlayerState(GameObject player)
        {
            Debug.Log("Binding PlayerState...");
            Container
                .Bind<PlayerState>()
                .FromInstance(player.GetComponent<PlayerState>());
        }

        private void BindPlayerRigidbody(GameObject player)
        {
            Debug.Log("Binding PlayerRigidbody...");
            Container
                .Bind<Rigidbody>()
                .WithId("player.rigidbody")
                .FromInstance(player.GetComponent<Rigidbody>());
        }

        private void BindPlayerGunBullet()
        {
            Debug.Log("Binding PlayerGunProjectile...");
            var projectile = Instantiate(Resources.Load("Projectiles/Bullet") as GameObject);
            Container
                .Bind<Rigidbody>()
                .WithId("player.weapon.gun.bullet")
                .FromInstance(projectile.GetComponent<Rigidbody>());
        }
    }
}
