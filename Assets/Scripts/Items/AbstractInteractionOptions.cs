﻿namespace DontGoAlone.Items
{
    public abstract class AbstractInteractionOptions
    {
        abstract class Builder <T>
        {
            public abstract T Build();
        }
    }
}