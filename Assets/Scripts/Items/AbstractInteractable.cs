﻿using DontGoAlone.Utils;
using System;
using UnityEngine;

namespace DontGoAlone.Items
{
    public abstract class AbstractInteractable : MonoBehaviour
    {
        public Guid Guid { get; set; }
        public Coordinates Coordinates { get; set; }

        protected AbstractInteractable()
        {
            Guid = Guid.NewGuid();
            Coordinates = new Coordinates();
        }

        public abstract void Interact();
    }
}