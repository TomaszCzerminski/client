﻿using DontGoAlone.Enemy;
using System.Collections;
using UnityEngine;

namespace DontGoAlone.Items
{
    class MissileManager : MonoBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            StartCoroutine(Destroy());
            if (collision.collider.CompareTag("Enemy"))
            {
                var state = collision.collider.GetComponent<EnemyState>();
                state.Health -= 1;
                Destroy(gameObject);
            }
        }

        private IEnumerator Destroy()
        {
            yield return new WaitForSeconds(0.2f);
            Destroy(gameObject);
        }
    }
}