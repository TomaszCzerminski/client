﻿using System;

namespace DontGoAlone.Items
{
    public class WeaponState
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public byte CurrentCapacity { get; set; }
        public byte MaxCapacity { get; set; }
        public byte Damage { get; set; }
        public byte Speed { get; set; }
    }
}
