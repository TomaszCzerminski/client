﻿using DontGoAlone.Enemy;
using System.Linq;
using UnityEngine;

namespace DontGoAlone.Items.Projectile
{
    class ProjectileController : MonoBehaviour
    {
        private void Update()
        {
            var enemies = Physics.OverlapSphere(transform.position, 0.1f)
                .Where(collider => collider.CompareTag("Enemy"))
                .ToArray();

            foreach(var enemy in enemies)
            {
                enemy.GetComponent<EnemyState>().Health -= 10;
            }
        }
    }
}
