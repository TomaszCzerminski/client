﻿using System.Collections.Generic;
using UnityEngine;

namespace DontGoAlone.Scene
{
    public class SceneState : MonoBehaviour
    {
        public IDictionary<string, GameObject> Players { get; set; }
        public IDictionary<string, GameObject> Enemies { get; set; }
        public IDictionary<string, GameObject> Interactables { get; set; }
        public byte Noise { get; set; }
    }
}