﻿using DontGoAlone.Enemy;
using DontGoAlone.Player.Actions;
using DontGoAlone.Player.Actions.Shoot;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Scene
{
    class SceneStateMutator : MonoBehaviour, IShootActionSubscriber
    {
        private SceneState sceneState;

        public void HandleShootAction(EnemyState enemy)
        {
            if (sceneState.Noise < 100)
            {
                sceneState.Noise += 10;
            }
        }

        [Inject]
        public void Inject(ActionDatabus databus, SceneState sceneState)
        {
            databus.Subscribe(Action.Shoot, this);
            this.sceneState = sceneState;
        }
    }
}
