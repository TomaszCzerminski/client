﻿using UnityEngine;
using Zenject;

namespace DontGoAlone.Scene
{
    class SceneComponentsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            var scene = GameObject.FindGameObjectWithTag("Scene");

            BindSceneState(scene);
        }

        private void BindSceneState(GameObject scene)
        {
            Container
                .Bind<SceneState>()
                .FromInstance(scene.GetComponent<SceneState>());
        }
    }
}
