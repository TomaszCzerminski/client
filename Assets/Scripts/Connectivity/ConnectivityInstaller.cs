﻿using System.Net.Sockets;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Connectivity
{
    class ConnectivityInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Debug.Log("Installing Connectivity bindings");

            Container.Bind<TcpClient>()
                .FromInstance(new TcpClient())
                .AsSingle();
        }
    }
}
