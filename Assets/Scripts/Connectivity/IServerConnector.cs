﻿namespace DontGoAlone.Connectivity
{
    interface IServerConnector
    {
        void Connect();
        void Send();
    }
}
