﻿using System.Net.Sockets;
using Zenject;

namespace DontGoAlone.Connectivity
{
    public class ServerConnector : IServerConnector
    {
        private readonly TcpClient client;

        public void Connect()
        {
            client.Connect("127.0.0.1", 11111);
        }

        public void Send()
        {
            var stream = client.GetStream();
            stream.Write(null, 0, 0);
        }

        [Inject]
        public ServerConnector(TcpClient client)
        {
            this.client = client;
        }
    }
}