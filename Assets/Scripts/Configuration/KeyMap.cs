﻿using UnityEngine;

namespace DontGoAlone
{
    public class KeyMap
    {
        private KeyMap()
        {
        }

        public readonly static KeyCode Right = KeyCode.D;
        public readonly static KeyCode Left = KeyCode.A;
        public readonly static KeyCode Up = KeyCode.W;
        public readonly static KeyCode Down = KeyCode.S;
        public readonly static KeyCode Jump = KeyCode.Space;
        public readonly static KeyCode Shoot = KeyCode.Mouse0;
        public readonly static KeyCode ToogleStealthMode = KeyCode.Q;
        public readonly static KeyCode ToogleAimingMode = KeyCode.Z;
        public readonly static KeyCode Interact = KeyCode.Mouse0;
        public readonly static KeyCode ToogleRunMode = KeyCode.R;
        public readonly static KeyCode SelectInteractable = KeyCode.Mouse0;
        public readonly static KeyCode SelectEnemy = KeyCode.Mouse0;
    }
}
