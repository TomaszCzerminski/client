﻿namespace DontGoAlone.Utils
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}