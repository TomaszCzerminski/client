﻿using System.Collections;
using UnityEngine;

namespace DontGoAlone.Utils
{
    class CoroutineExecutor : MonoBehaviour
    {
        public void Execute(IEnumerator generator)
        {
            StartCoroutine(generator);
        }
    }
}
