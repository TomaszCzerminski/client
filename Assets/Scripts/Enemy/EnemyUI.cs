﻿using UnityEngine;
using UnityEngine.UI;

namespace DontGoAlone.Enemy
{
    class EnemyUI : MonoBehaviour
    {
        public Slider slider;
        private EnemyState state;     

        private void Awake()
        {
            state = GetComponent<EnemyState>();
        }
    }
}
