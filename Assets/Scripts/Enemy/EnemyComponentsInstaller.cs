﻿using UnityEngine;
using Zenject;

namespace DontGoAlone.Enemy
{
    class EnemyComponentsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        { 
            var enemy = GameObject.FindGameObjectWithTag("Enemy");

            BindEnemyState(enemy);
            BindEnemyAnimator(enemy);
        }

        private void BindEnemyState(GameObject enemy)
        {
            Container
                .Bind<EnemyState>()
                .FromInstance(enemy.GetComponent<EnemyState>());
        }

        private void BindEnemyAnimator(GameObject enemy)
        {
            Container
                .Bind<Animator>()
                .WithId("enemy.animator")
                .FromInstance(enemy.GetComponent<Animator>());
        }
    }
}
