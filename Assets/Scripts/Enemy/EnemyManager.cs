﻿using DontGoAlone.Player;
using DontGoAlone.Player.Events;
using DontGoAlone.Player.Events.TakeDamage;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Enemy
{
    public class EnemyManager : MonoBehaviour
    {
        public Texture2D attackCursor;
        public Texture2D defaultCursor;
        private PlayerState playerState;
        private Animator animator;
        private EnemyState enemyState;
        private readonly WaitForSeconds interval = new WaitForSeconds(1);
        private TakeDamageEventPublisher takeDamageEventPublisher;

        private bool CanAttack { get; set; }

        private void OnMouseEnter()
        {
            playerState.HoveredEnemy = enemyState;
            Cursor.SetCursor(attackCursor, Vector2.zero, CursorMode.Auto);
        }

        private void OnMouseExit()
        {
            playerState.HoveredEnemy = null;
            Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
        }

        void FixedUpdate()
        {
            if (enemyState.Health <= 0)
            {
                Die();
                return;
            }
            else
            {
                var players = DetectPlayers();
                var target = FindNearest(players);

                if (target != null)
                {
                    transform.LookAt(
                        new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
                    if (CanAttack && IsPlayerInAttackRange(target) && !playerState.IsDead)
                    {
                        if (takeDamageEventPublisher.CanPublish())
                            takeDamageEventPublisher.Publish();
                        CanAttack = false;
                        StartCoroutine(WaitUntilNextAttack());
                    }
                    else
                    {

                        var movement = transform.position.z > target.transform.position.z ? -20f : 20f;
                        GetComponent<Rigidbody>().AddForce(
                            new Vector3(0, 0, movement));
                    }
                }
                else
                {
                    var playersToPursue = FindPlayersToPursue();
                }
            }
        }

        private IEnumerator WaitUntilNextAttack()
        {
            yield return interval;
            CanAttack = true;
        }

        private Collider[] FindPlayersToPursue()
        {
            return Physics
                .OverlapSphere(transform.position, 15)
                .Where(collider => collider.CompareTag(playerState.Tag))
                .ToArray();
        }

        private Collider FindNearest(Collider[] colliders)
        {
            Collider nearest = null;
            float minimalDistance = float.MaxValue;

            foreach (var collider in colliders)
            {
                var distance = Vector3.Distance(transform.position, collider.transform.position);

                if (distance < minimalDistance)
                {
                    nearest = collider;
                    minimalDistance = distance;
                }
            }

            return nearest;
        }

        private bool IsPlayerInAttackRange(Collider collider)
        {
            return Physics
                .OverlapSphere(transform.position, 1)
                .Any(it => it.Equals(collider));
        }

        private Collider[] DetectPlayers()
        {
            return Physics
                .OverlapSphere(transform.position, 5)
                .Where(collider => collider.CompareTag(playerState.Tag))
                .ToArray();
        }

        private void Die()
        {
            Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
            Destroy(gameObject);
            Destroy(transform.parent.gameObject);
            enemyState.IsDead = true;
        }

        [Inject]
        public void Inject(
            EventDatabus eventDatabus,
            PlayerState playerState,
            EnemyState enemyState,
            [Inject(Id = "enemy.animator")] Animator animator)
        {
            CanAttack = true;
            this.enemyState = enemyState;
            this.playerState = playerState;
            this.animator = animator;
            takeDamageEventPublisher = eventDatabus.GetPublisher(Player.Events.Event.TakeDamage) as TakeDamageEventPublisher;
        }
    }
}