﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMenuController : MonoBehaviour {
    Animator anim;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.tag.Equals("Enemy") || transform.tag.Equals("EnemyEeating"))
        transform.Translate(Vector3.forward * Time.deltaTime);
       
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("EnemyEeating"))
        {
            anim = other.GetComponent<Animator>();
            anim.SetBool("New Bool", true);
            other.tag = "Untagged";
        }
    }
}
