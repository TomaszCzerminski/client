﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnZombie : MonoBehaviour
{
    public List<GameObject> zombieList = new List<GameObject>();
   
    private void OnTriggerExit(Collider other)
    {
        Instantiate<GameObject>(
            zombieList[Random.Range(0,12)],
            new Vector3(
                other.transform.position.x + Random.Range(15, 20),
                other.transform.position.y,
                other.transform.position.z + Random.Range(-2, 2)),
            Quaternion.Euler(0f, -90f, 0f));
    }
}
