﻿using DontGoAlone.Player;
using DontGoAlone.Utils;
using System;
using UnityEngine;

namespace DontGoAlone.Enemy
{
    public class EnemyState : MonoBehaviour
    {
        private void Awake()
        {
            Health = 100;
        }
        public Guid Guid { get; set; }
        public bool IsDead { get; set; }
        public byte Range { get; set; }
        public byte Health { get; set; }
        public float WalkingSpeed { get; set; }
        public bool IsFacingLeft { get; set; }
        public Coordinates Coordinates { get; set; }
        public PlayerState[] PlayersSpotted { get; set; }
        public PlayerState[] PlayersInRange { get; set; }
    }
}