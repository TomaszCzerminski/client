﻿using UnityEngine;

namespace DontGoAlone.Interface
{
    public class CursorController : MonoBehaviour
    {
        public Texture2D texture;

        private void Awake()
        {
            Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
        }
    }
}