﻿using DontGoAlone.Player;
using DontGoAlone.Scene;
using UnityEngine;
using Zenject;

namespace DontGoAlone.Interface
{
    public class HUD : MonoBehaviour
    {
        private static Rect STAMINA_INDICATOR = new Rect(10, 10, 0, 0);
        private static Rect PAIN_INDICATOR = new Rect(10, 30, 0, 0);
        private static Rect NOISE_INDICATOR = new Rect(10, 50, 0, 0);

        private PlayerState playerState;
        private SceneState sceneState;
        public Texture2D painTexture;
        public Texture2D staminaTexture;
        public Texture2D noiseTexture;
        private float barWidth;
        private static float barHeight;

        private void OnGUI()
        {
            STAMINA_INDICATOR.width = playerState.Stamina * barWidth / 100;
            STAMINA_INDICATOR.height = barHeight;

            PAIN_INDICATOR.width = playerState.Pain * barWidth / 100;
            PAIN_INDICATOR.height = barHeight;

            NOISE_INDICATOR.width = sceneState.Noise * barWidth / 100;
            NOISE_INDICATOR.height = barHeight;

            GUI.DrawTexture(STAMINA_INDICATOR, staminaTexture);
            GUI.DrawTexture(PAIN_INDICATOR, painTexture);
            GUI.DrawTexture(NOISE_INDICATOR, noiseTexture);
        }

        private void Awake()
        {
            playerState = GameObject
                .FindGameObjectWithTag("Player")
                .GetComponent<PlayerState>();
            sceneState = GameObject
                .FindGameObjectWithTag("Scene")
                .GetComponent<SceneState>();
            barHeight = Screen.height * 0.02f;
            barWidth = barHeight * 30.0f;
        }
    }
}